from __future__ import unicode_literals

from django.apps import AppConfig

from hardie.django_utils import ucgettext_lazy as _


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = _('users')
