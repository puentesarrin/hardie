from django.contrib import admin
from django.utils.translation import ugettext as _

from users.models import Classroom, StudentProfile, TeacherProfile


class StudentProfileInline(admin.StackedInline):

    model = StudentProfile
    verbose_name = _('student profile')
    verbose_name_plural = verbose_name  # Intentionally in singular since it is a one-to-one-relationship

class TeacherProfileInline(admin.StackedInline):

    model = TeacherProfile
    verbose_name = _('teacher profile')
    verbose_name_plural = verbose_name  # Intentionally in singular since it is a one-to-one-relationship


class ClassroomInline(admin.TabularInline):

    model = Classroom
    extra = 0
