from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext as _

from users.models import HardieUser, Grade

from .inlines import ClassroomInline, StudentProfileInline, TeacherProfileInline
from .forms import UserChangeForm, UserCreationForm


class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('dni', 'last_name', 'first_name', 'email', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (_('Personal info'), {'fields': ('dni', 'first_name', 'last_name', 'email')}),
        (_('Status and permissions'), {'fields': ('is_active', 'is_admin')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('dni', 'password1', 'password2', 'email')
        }),
    )
    search_fields = ('dni', 'first_name', 'last_name')
    ordering = ('last_name',)
    filter_horizontal = ()
    inlines = (StudentProfileInline, TeacherProfileInline)


@admin.register(Grade)
class GradeAdmin(admin.ModelAdmin):

    list_display = ('name', 'education_level')
    list_filter = ('education_level',)
    search_fields = ('name', 'number',)
    ordering = ('education_level', 'number')
    inlines = (ClassroomInline,)


admin.site.register(HardieUser, UserAdmin)
admin.site.unregister(Group)
