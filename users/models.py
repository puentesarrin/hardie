from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils.translation import ugettext as _

INITIAL_LEVEL = 0
PRIMARY_LEVEL = 1
SECONDARY_LEVEL = 2
EDUCATION_LEVEL_CHOICES = (
    (INITIAL_LEVEL, _('Initial level')),
    (PRIMARY_LEVEL, _('Primary level')),
    (SECONDARY_LEVEL, _('Secondary level')),
)
EDUCATION_LEVEL_CHOICES_DICT = dict(EDUCATION_LEVEL_CHOICES)


def get_education_level_name(education_level_value):
    return EDUCATION_LEVEL_CHOICES_DICT[education_level_value]


class HardieUserManager(BaseUserManager):

    def create_user(self, dni, first_name, last_name, password=None):
        user = self.model(dni=dni, first_name=first_name, last_name=last_name)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, dni, first_name, last_name, password):
        user = self.create_user(dni, first_name, last_name, password=password)
        user.is_admin = True
        user.save()
        return user


class HardieUser(AbstractBaseUser, PermissionsMixin):

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    USERNAME_FIELD = 'dni'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = HardieUserManager()

    dni = models.CharField(unique=True, max_length=8, blank=False, verbose_name=_('DNI'))
    email = models.EmailField(max_length=255, null=True, blank=True, verbose_name=_('email address'))
    first_name = models.CharField(max_length=200, blank=False, null=False, verbose_name=_('first name'))
    last_name = models.CharField(max_length=200, blank=False, null=False, verbose_name=_('last name'))
    is_active = models.BooleanField(default=True, verbose_name=_('active'))
    is_admin = models.BooleanField(default=False, verbose_name=_('admin'))

    @property
    def is_staff(self):
        return self.is_admin

    def __get_profile(self, ProfileModel):
        try:
            return ProfileModel.objects.get(pk=self.pk)
        except ProfileModel.DoesNotExist:
            return None

    @property
    def student_profile(self):
        if not hasattr(self, '__student_profile'):
            self.__student_profile = self.__get_profile(StudentProfile)
        return self.__student_profile

    @property
    def teacher_profile(self):
        if not hasattr(self, '__teacher_profile'):
            self.__teacher_profile = self.__get_profile(TeacherProfile)
        return self.__teacher_profile

    def get_full_name(self):
        return u'{} {}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    def __unicode__(self):
        return self.dni


class Profile(models.Model):

    class Meta:
        abstract = True
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    user = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, on_delete=models.CASCADE)


class Grade(models.Model):

    class Meta:
        unique_together = (
            ('education_level', 'name'),
            ('education_level', 'number'),
        )
        verbose_name = _('grade')
        verbose_name_plural = _('grades')

    education_level = models.IntegerField(choices=EDUCATION_LEVEL_CHOICES, verbose_name=_('education level'))
    name = models.CharField(max_length=50, verbose_name=_('name'))
    number = models.IntegerField(verbose_name=_('numeric representation'))

    def __unicode__(self):
        return u'{} - {}'.format(get_education_level_name(self.education_level), self.name)


class Classroom(models.Model):

    class Meta:
        unique_together = ('grade', 'name')
        ordering = ('grade__education_level', 'grade__number', 'name')
        verbose_name = _('classroom')
        verbose_name_plural = _('classrooms')

    grade = models.ForeignKey(Grade, verbose_name=_('grade'), on_delete=models.CASCADE)
    name = models.CharField(max_length=50, verbose_name=_('name'))

    def __unicode__(self):
        return u'{} "{}"'.format(self.grade, self.name)


class StudentProfile(Profile):

    class Meta:
        verbose_name = _('student profile')
        verbose_name_plural = _('student profiles')

    classroom = models.ForeignKey(Classroom, verbose_name=_('classroom'), on_delete=models.PROTECT)
    year = models.PositiveSmallIntegerField(verbose_name=_('year'))

    def __unicode__(self):
        return _(u'student of {}').capitalize().format(self.classroom)


class TeacherProfile(Profile):

    class Meta:
        verbose_name = _('teacher profile')
        verbose_name_plural = _('teacher profiles')

    education_level = models.IntegerField(choices=EDUCATION_LEVEL_CHOICES, verbose_name=_('education level'))

    def __unicode__(self):
        return _(u'teacher of {}').capitalize().format(get_education_level_name(self.education_level))
