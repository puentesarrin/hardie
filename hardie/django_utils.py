from django.utils import six
from django.utils.functional import lazy
from django.utils.translation import ugettext


def _unicode_capitalize(string):
    return ugettext(string).capitalize()


ucgettext_lazy = lazy(_unicode_capitalize, six.text_type)
