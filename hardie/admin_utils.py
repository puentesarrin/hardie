class SearchByIdMixin(object):

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(
            SearchByIdMixin,
            self
        ).get_search_results(
            request,
            queryset,
            search_term
        )
        try:
            search_term_as_int = int(search_term)
        except ValueError:
            pass
        else:
            queryset |= self.model.objects.filter(**{
                self.search_by_id_field: search_term_as_int
            })
        return queryset, use_distinct
