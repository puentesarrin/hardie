from __future__ import unicode_literals

from datetime import datetime

from django.conf import settings
from django.core import exceptions
from django.db import models
from django.utils.translation import ugettext as _


class Country(models.Model):

    class Meta:
        verbose_name = _('country')
        verbose_name_plural = _('countries')

    name = models.CharField(max_length=200, unique=True, verbose_name=_('name'))

    def __unicode__(self):
        return self.name


class Author(models.Model):

    class Meta:
        verbose_name = _('author')
        verbose_name_plural = _('authors')

    first_name = models.CharField(max_length=200, verbose_name=_('first name'))
    last_name = models.CharField(max_length=200, blank=True, verbose_name=_('last name'))
    country = models.ForeignKey(Country, blank=True, null=True, verbose_name=_('country'), on_delete=models.SET_NULL)

    def get_short_name(self):
        return self.last_name

    def get_full_name(self):
        return u'{} {}'.format(self.first_name, self.last_name)
    get_full_name.short_description = _('full name')

    full_name = property(get_full_name)

    def __unicode__(self):
        return self.full_name


class PublishingCompany(models.Model):

    class Meta:
        verbose_name = _('publishing company')
        verbose_name_plural = _('publishing companies')
        unique_together = ('name', 'country')

    name = models.CharField(max_length=200, verbose_name=_('name'))
    country = models.ForeignKey(Country, blank=True, null=True, verbose_name=_('country'), on_delete=models.SET_NULL)

    def __unicode__(self):
        return self.name


class BookCategory(models.Model):

    class Meta:
        verbose_name = _('book category')
        verbose_name_plural = _('book categories')

    name = models.CharField(max_length=200, unique=True, verbose_name=_('name'))

    def __unicode__(self):
        return self.name


class Book(models.Model):

    class Meta:
        verbose_name = _('book')
        verbose_name_plural = _('books')

    title = models.CharField(max_length=500, verbose_name=_('title'))
    authors = models.ManyToManyField(Author, verbose_name=_('authors'))
    edition = models.CharField(max_length=5, blank=True, null=True, verbose_name=_('edition'))
    isbn = models.CharField(max_length=20, blank=True, null=True, verbose_name=_('ISBN'))
    category = models.ForeignKey(BookCategory, verbose_name=_('category'), on_delete=models.PROTECT)
    publishing_company = models.ForeignKey(PublishingCompany, verbose_name=_('publishing company'),
                                           on_delete=models.PROTECT)
    publishing_year = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name=_('publishing year'))
    additional_information = models.CharField(max_length=1000, blank=True, null=True,
                                              verbose_name=_('additional information'))

    def __unicode__(self):
        return self.title


class BookCopy(models.Model):

    class Meta:
        verbose_name = _('book copy')
        verbose_name_plural = _('book copies')

    STATUS_AVAILABLE = 0
    STATUS_BORROWED = 1
    STATUS_DEPRECATED = 2
    STATUS_CHOICES = (
        (STATUS_AVAILABLE, _('Available')),
        (STATUS_BORROWED, _('Borrowed')),
        (STATUS_DEPRECATED, _('Deprecated'))
    )

    CONDITION_GOOD = 0
    CONDITION_DEPRECATED = 1
    CONDITION_CHOICES = (
        (CONDITION_GOOD, _('Good')),
        (CONDITION_DEPRECATED, _('Deprecated')),
    )

    code = models.CharField(max_length=10, unique=True, verbose_name=_('code'))
    book = models.ForeignKey(Book, verbose_name=_('book'), on_delete=models.PROTECT)
    additional_information = models.CharField(max_length=1000, blank=True, null=True,
                                              verbose_name=_('additional information'),)
    condition = models.IntegerField(choices=CONDITION_CHOICES, default=CONDITION_GOOD, verbose_name=_('condition'))

    @property
    def is_borrowed(self):
        count = BookOutOnLoan.objects.filter(
            book_copy=self,
            status=BookOutOnLoan.STATUS_NOT_RETURNED,
        ).count()
        return bool(count)

    @property
    def is_available(self):
        if self.condition == self.CONDITION_DEPRECATED:
            return False
        return not self.is_bollowed

    @property
    def status_id(self):
        if self.condition == self.CONDITION_DEPRECATED:
            return self.STATUS_DEPRECATED
        return (
            self.STATUS_BORROWED if self.is_borrowed else self.STATUS_AVAILABLE
        )

    def status(self):
        return dict(self.STATUS_CHOICES)[self.status_id]
    status.short_description = _('status')
    status = property(status)

    def __unicode__(self):
        return u'{} - {}'.format(self.code, self.book.title)


class BookOutOnLoan(models.Model):

    class Meta:
        verbose_name = _('book out on load')
        verbose_name_plural = _('books out on load')

    STATUS_NOT_RETURNED = 0
    STATUS_RETURNED = 1
    STATUS_CHOICES = (
        (STATUS_NOT_RETURNED, _('Not returned')),
        (STATUS_RETURNED, _('Returned')),
    )

    book_copy = models.ForeignKey(BookCopy, verbose_name=_('book copy'))
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user'))
    date_issued = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('date issued'))
    date_due_to_return = models.DateField(default=datetime.now, verbose_name=_('date due to return'))
    date_returned = models.DateField(blank=True, null=True, verbose_name=_('date returned'))
    status = models.IntegerField(choices=STATUS_CHOICES, default=STATUS_NOT_RETURNED, verbose_name=_('status'))
    additional_information = models.CharField(max_length=1000, blank=True, verbose_name=_('additional information'))

    def clean(self):
        if self.pk:
            if self.date_returned and self.status != BookOutOnLoan.STATUS_RETURNED:
                raise exceptions.ValidationError({
                    'status': _('Status must be "{}" if you set date returned').format(_('Returned'))
                })
            elif self.status == BookOutOnLoan.STATUS_RETURNED and not self.date_returned:
                raise exceptions.ValidationError({
                    'date_returned': _('Date returned is required when returning book copies')
                })

    def __unicode__(self):
        return _(u'{} borrowed to {}').format(self.book_copy, self.user)
