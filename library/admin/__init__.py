from django.contrib import admin
from daterange_filter.filter import DateRangeFilter

from hardie.admin_utils import SearchByIdMixin
from library.models import Author, Book, BookCategory, BookOutOnLoan, Country, PublishingCompany

from .inlines import BookCopyInline


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):

    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):

    search_fields = ('first_name', 'last_name')
    list_filter = ('country',)
    list_display = ('full_name', 'country')
    ordering = ('first_name', 'last_name')


@admin.register(PublishingCompany)
class PublishingCompanyAdmin(admin.ModelAdmin):

    search_fields = ('name',)
    list_filter = ('country',)
    list_display = ('name', 'country')
    ordering = ('name',)


@admin.register(BookCategory)
class BookCategoryAdmin(admin.ModelAdmin):

    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):

    search_fields = ('title', 'authors__first_name', 'authors__last_name')
    filter_horizontal = ('authors',)
    list_filter = ('category', 'publishing_company')
    list_display = ('title', 'edition', 'category', 'publishing_company', 'publishing_year', 'isbn')
    ordering = ('title',)
    inlines = (BookCopyInline,)


@admin.register(BookOutOnLoan)
class BookOutOnLoanAdmin(SearchByIdMixin, admin.ModelAdmin):

    fields = ('book_copy', 'user', 'date_due_to_return', 'date_returned', 'status', 'additional_information')
    search_fields = ('book_copy__book__title', 'user__id', 'user__username')
    search_by_id_field = 'book_copy__id'
    list_display = ('id', 'book_copy', 'user', 'date_issued', 'date_due_to_return', 'date_returned', 'status')
    list_filter = (
        'status',
        ('date_issued', DateRangeFilter),
        ('date_due_to_return', DateRangeFilter),
        ('date_returned', DateRangeFilter),
    )
    raw_id_fields = ('book_copy', 'user')
    ordering = ('-date_due_to_return', 'date_issued')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('book_copy', 'user',)
        return self.readonly_fields
