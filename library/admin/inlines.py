from django.contrib import admin

from library.models import BookCopy


class BookCopyInline(admin.TabularInline):

    model = BookCopy
    extra = 0
    radio_fields = {
        'condition': admin.HORIZONTAL,
    }
